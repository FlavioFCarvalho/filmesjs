(function(){
	angular
	  .module('filmes')
	  .controller('FilmesController', function($scope, MeusFilmes) {
		$scope.titulo = "Filmes que já assisti";
  
		$scope.filmes = [ ];
	
		MeusFilmes.listar().then(function(filmes){
			$scope.filmes = filmes;
		});

		$scope.novoFilme = {};

		$scope.resetForm = function() {
			$scope.formulario.$setPristine();
			$scope.formulario.$setUntouched();
		  }
	
  
		$scope.criarFilme = function() {
			$scope.filmes.push({
				id: Date.now() ,
				titulo: $scope.novoFilme.titulo,
				ano: $scope.novoFilme.ano,
				produtora: $scope.novoFilme.produtora,
				sinopse: $scope.novoFilme.sinopse,
				cartaz: $scope.novoFilme.cartaz
			});
  
			$scope.novoFilme = {};
		}
  
		$scope.removerFilme = function(id) {
			angular.forEach($scope.filmes, function(filme, i){
				if(filme.id == id){
					$scope.filmes.splice(i, 1);
				};
			});
		}
  
	  });
  })();
