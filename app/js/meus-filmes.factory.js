angular.module("filmes").factory("MeusFilmes", function($q, $http){
    return{
        listar: function(){
            var promessa = $q.defer();
            $http.get("https://meus-filmes-c3d68.firebaseio.com/filmes.json").then(
                function(result){
                    console.log(result);
                    var filmes = [];

                    angular.forEach(result.data, function(filme, id){
                        filme.id = id;
                        filmes.push(filme);
                    });

                    promessa.resolve(filmes);
                }
            )

            return promessa.promise; 
        }
    };
});